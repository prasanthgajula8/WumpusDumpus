﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletMover : MonoBehaviour
{
    public float MovementSpeed;
    float m_distanceTraveled = 0f;
    Collider col;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (m_distanceTraveled < 3.3f)
        {
            Vector3 oldPosition = transform.position;
            transform.Translate(Vector3.right * MovementSpeed * Time.deltaTime);
            m_distanceTraveled += Vector3.Distance(oldPosition, transform.position);
        }
        else
        {
            if(col.gameObject.tag=="Wumpus")
            {
                Color temp = col.GetComponent<SpriteRenderer>().color;
                temp.a = 0f;
                col.GetComponent<SpriteRenderer>().color = temp;
                col.GetComponent<BoxCollider>().enabled = false;
                PlayerController.KilledWumpus = true;
                GameObject.Find("Alerts").GetComponent<Text>().text = "Success!";
            }
            else
            {
                PlayerController.KilledWumpus = false;
                GameObject.Find("Alerts").GetComponent<Text>().text = "You Failed!";
            }
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        col = other;
    }
}
