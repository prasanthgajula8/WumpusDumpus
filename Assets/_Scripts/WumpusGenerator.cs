﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WumpusGenerator : MonoBehaviour
{
    int RandomNumber;
    int RandomNumberPit;
    int RandomNumberGold;
    int i;
    string RandomString;
    public GameObject Wumpus;
    public GameObject WumpusDown;
    public GameObject WumpusLeft;
    public GameObject WumpusNE;
    public GameObject WumpusNW;
    public GameObject WumpusRight;
    public GameObject WumpusSE;
    public GameObject WumpusUp;
    public GameObject Pit;
    public GameObject PitDown;
    public GameObject PitLeft;
    public GameObject PitNE;
    public GameObject PitNW;
    public GameObject PitRight;
    public GameObject PitSE;
    public GameObject PitUp;
    public GameObject Gold;
    public GameObject Hero;
    GameObject Block;

    // Use this for initialization
    void Start()
    {
        FindObjectOfType<FillScript>().IsFilled = false;
        Instantiate(Hero, GameObject.Find("1").transform);
        RandomWumpusGenerator();
        RandomGoldGenerator();
        Block.GetComponent<FillScript>().IsFilled = true;
        for (i = 0; i <= 2; i++)
        {
            RandomPitGenerator();
        }
        Block.GetComponent<FillScript>().IsFilled = true;
    }

    void RandomWumpusGenerator()
    {
        RandomNumber = Random.Range(2, 16);
        RandomString = RandomNumber.ToString();
        Block = GameObject.Find(RandomString);
        switch (RandomNumber)
        {
            case 2: Instantiate(WumpusUp, Block.transform); break;
            case 3: Instantiate(WumpusUp, Block.transform); break;
            case 4: Instantiate(WumpusSE, Block.transform); break;
            case 5: Instantiate(WumpusRight, Block.transform); break;
            case 6: Instantiate(Wumpus, Block.transform); break;
            case 7: Instantiate(Wumpus, Block.transform); break;
            case 8: Instantiate(WumpusLeft, Block.transform); break;
            case 9: Instantiate(WumpusRight, Block.transform); break;
            case 10: Instantiate(Wumpus, Block.transform); break;
            case 11: Instantiate(Wumpus, Block.transform); break;
            case 12: Instantiate(WumpusLeft, Block.transform); break;
            case 13: Instantiate(WumpusNW, Block.transform); break;
            case 14: Instantiate(WumpusDown, Block.transform); break;
            case 15: Instantiate(WumpusDown, Block.transform); break;
            case 16: Instantiate(WumpusNE, Block.transform); break;
        }
        Block.GetComponent<FillScript>().IsFilled = true;
    }

    void RandomPitGenerator()
    {
        if (RandomNumber < RandomNumberGold)
        {
            if (i == 0)
            {
                RandomNumberPit = Random.Range(2, RandomNumber - 1);
            }

            else if (i == 1)
            {
                RandomNumberPit = Random.Range(RandomNumber + 1, RandomNumberGold - 1);
            }

            else
            {
                RandomNumberPit = Random.Range(RandomNumberGold + 1, 16);
            }
        }

        else
        {
            if (i == 0)
            {
                RandomNumberPit = Random.Range(3, RandomNumberGold - 1);
            }

            else if (i == 1)
            {
                RandomNumberPit = Random.Range(RandomNumberGold + 1, RandomNumber - 1);
            }

            else
            {
                RandomNumberPit = Random.Range(RandomNumber + 1, 16);
            }
        }

        RandomString = RandomNumberPit.ToString();
        Block = GameObject.Find(RandomString);
        switch (RandomNumberPit)
        {
            case 2:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(PitUp, Block.transform);
                }
                break;
            case 3:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(PitUp, Block.transform);
                }
                break;
            case 4:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(PitSE, Block.transform);
                }
                break;
            case 5:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(PitRight, Block.transform);
                }
                break;
            case 6:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Pit, Block.transform);
                }
                break;
            case 7:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Pit, Block.transform);
                }
                break;
            case 8:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(PitLeft, Block.transform);
                }
                break;
            case 9:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(PitRight, Block.transform);
                }
                break;
            case 10:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Pit, Block.transform);
                }
                break;
            case 11:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Pit, Block.transform);
                }
                break;
            case 12:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(PitLeft, Block.transform);
                }
                break;
            case 13:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(PitNW, Block.transform);
                }
                break;
            case 14:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(PitDown, Block.transform);
                }
                break;
            case 15:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(PitDown, Block.transform);
                }
                break;
            case 16:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(PitNE, Block.transform);
                }
                break;
        }
    }

    void RandomGoldGenerator()
    {
        if (RandomNumber == 2)
        {
            RandomNumberGold = Random.Range(RandomNumber + 1, 16);
        }
        else
        {
            RandomNumberGold = Random.Range(2, RandomNumber-1);
        }      
        RandomString = RandomNumberGold.ToString();
        Block = GameObject.Find(RandomString);

        switch (RandomNumberGold)
        {
            case 2: 
                if(!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
            case 3:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
            case 4:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
            case 5:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
            case 6:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
            case 7:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
            case 8:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
            case 9:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
            case 10:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }               
                break;
            case 11:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
            case 12:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
            case 13:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
            case 14:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
            case 15:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
            case 16:
                if (!Block.GetComponent<FillScript>().IsFilled)
                {
                    Instantiate(Gold, Block.transform);
                }
                break;
        }
    }

    public void RePlay()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
