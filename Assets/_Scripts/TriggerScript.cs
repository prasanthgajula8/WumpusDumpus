﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    public bool Right, Left, Up, Down;
    public static bool canMoveRight, canMoveLeft, canMoveUp, canMoveDown;
    // Use this for initialization
    void Start()
    {
        canMoveRight = false;
        canMoveLeft = false;
        canMoveUp = false;
        canMoveDown = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (PlayerController.LoopCompleted)
        {
            if (other.gameObject.tag == "Wumpus" || other.gameObject.tag == "Pit" || other.gameObject.tag == "Trigger")
            {
                if (Right)
                {
                    canMoveRight = false;
                    if (other.gameObject.tag == "Wumpus" && PlayerController.AIisRunning)
                        FindObjectOfType<PlayerController>().ShootRight();
                }

                if (Left)
                {
                    canMoveLeft = false;
                    if (other.gameObject.tag == "Wumpus" && PlayerController.AIisRunning)
                        FindObjectOfType<PlayerController>().ShootLeft();
                }

                if (Up)
                {
                    canMoveUp = false;
                    if (other.gameObject.tag == "Wumpus" && PlayerController.AIisRunning)
                        FindObjectOfType<PlayerController>().ShootUp();
                }

                if (Down)
                {
                    canMoveDown = false;
                    if (other.gameObject.tag == "Wumpus" && PlayerController.AIisRunning)
                        FindObjectOfType<PlayerController>().ShootDown();
                }
            }

            else
            {
                if (Right)
                {
                    canMoveRight = true;
                }

                if (Left)
                {
                    canMoveLeft = true;
                }

                if (Up)
                {
                    canMoveUp = true;
                }

                if (Down)
                {
                    canMoveDown = true;
                }
            }
        }
    }
}
