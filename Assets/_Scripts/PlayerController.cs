﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Transform parent;
    public static int score;
    public int MoveScore;
    public int WumpusKill;
    public int GoldScore,i;
    public static bool GrabbedGold, KilledWumpus,AIisRunning,LoopCompleted;
    GameObject[] GO;
    int RandomNumber;
    // Use this for initialization
    void Start()
    {
        i = 0;
        GrabbedGold = false;
        KilledWumpus = false;
        AIisRunning = false;
        LoopCompleted = true;
        score = 0;
        parent = GameObject.Find("1").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown("w"))
        {
            MoveUp();
        }

        if (Input.GetKeyDown("a"))
        {
            MoveLeft();
        }

        if (Input.GetKeyDown("s"))
        {
            MoveDown();
        }

        if (Input.GetKeyDown("d"))
        {
            MoveRight();
        }

        if (Input.GetKeyDown("j"))
        {
            ShootLeft();
        }

        if (Input.GetKeyDown("i"))
        {
            ShootUp();
        }

        if (Input.GetKeyDown("l"))
        {
            ShootRight();
        }

        if (Input.GetKeyDown("k"))
        {
            ShootDown();
        }
    }

    public void MoveUp()
    {
        if(this.transform.position.y<6.03299)
        {
            this.transform.Translate(0f, 3.2944f, 0f,parent);
            score -= MoveScore;
            GameObject.Find("Score").GetComponent<Text>().text = score.ToString();
            TriggerScript.canMoveDown = false;
            TriggerScript.canMoveLeft = false;
            TriggerScript.canMoveRight = false;
            TriggerScript.canMoveUp = false;
        }
    }

    public void MoveLeft()
    {
        if (this.transform.position.x>-4)
        {
            this.transform.Translate(-3.2944f, 0f, 0f,parent);
            score -= MoveScore;
            GameObject.Find("Score").GetComponent<Text>().text = score.ToString();
            TriggerScript.canMoveDown = false;
            TriggerScript.canMoveLeft = false;
            TriggerScript.canMoveRight = false;
            TriggerScript.canMoveUp = false;
        }
    }

    public void MoveDown()
    {
        if (this.transform.position.y > -3)
        {
            this.transform.Translate(0f, -3.2944f, 0f,parent);
            score -= MoveScore;
            GameObject.Find("Score").GetComponent<Text>().text = score.ToString();
            TriggerScript.canMoveDown = false;
            TriggerScript.canMoveLeft = false;
            TriggerScript.canMoveRight = false;
            TriggerScript.canMoveUp = false;
        }
    }

    public void MoveRight()
    {
        if (this.transform.position.x < 5.45299)
        {
            this.transform.Translate(3.2944f, 0f, 0f,parent);
            score -= MoveScore;
            GameObject.Find("Score").GetComponent<Text>().text = score.ToString();
            TriggerScript.canMoveDown = false;
            TriggerScript.canMoveLeft = false;
            TriggerScript.canMoveRight = false;
            TriggerScript.canMoveUp = false;
        }
    }

    public void ShootRight()
    {
        this.transform.GetChild(0).gameObject.SetActive(true);
        GameObject.Find("Bullet").transform.eulerAngles = new Vector3(0, 0, 0);
    }

    public void ShootLeft()
    {
        this.transform.GetChild(0).gameObject.SetActive(true);
        GameObject.Find("Bullet").transform.eulerAngles = new Vector3(0, 0, 180f);
    }

    public void ShootUp()
    {
        this.transform.GetChild(0).gameObject.SetActive(true);
        GameObject.Find("Bullet").transform.eulerAngles = new Vector3(0, 0, 90f);
    }

    public void ShootDown()
    {
        this.transform.GetChild(0).gameObject.SetActive(true);
        GameObject.Find("Bullet").transform.eulerAngles = new Vector3(0, 0, 270f);
    }

    //ienumerator executeaftertime(float time)
    //{
    //    yield return new waitforseconds(time);
    //}

    public IEnumerator RunAI()
    {
        while(!KilledWumpus||!GrabbedGold)
        {
            AIisRunning = true;
            yield return new WaitForSeconds(.5f);

            if (TriggerScript.canMoveLeft && !TriggerScript.canMoveRight && !TriggerScript.canMoveUp && !TriggerScript.canMoveDown)
            {
                MoveLeft();
            }

            else if (!TriggerScript.canMoveLeft && TriggerScript.canMoveRight && !TriggerScript.canMoveUp && !TriggerScript.canMoveDown)
            {
                MoveRight();
            }

            else if (!TriggerScript.canMoveLeft && !TriggerScript.canMoveRight && TriggerScript.canMoveUp && !TriggerScript.canMoveDown)
            {
                MoveUp();
            }

            else if (!TriggerScript.canMoveLeft && !TriggerScript.canMoveRight && !TriggerScript.canMoveUp && TriggerScript.canMoveDown)
            {
                MoveDown();
            }

            else if (TriggerScript.canMoveLeft && TriggerScript.canMoveRight && !TriggerScript.canMoveUp && !TriggerScript.canMoveDown)
            {
                RandomNumber = Random.Range(0, 1);
                if (RandomNumber == 0)
                {
                    MoveLeft();
                }
                else
                {
                    MoveRight();
                }
            }

            else if (!TriggerScript.canMoveLeft && TriggerScript.canMoveRight && TriggerScript.canMoveUp && !TriggerScript.canMoveDown)
            {
                RandomNumber = Random.Range(0, 1);
                if (RandomNumber == 0)
                {
                    MoveUp();
                }
                else
                {
                    MoveRight();
                }
            }

            else if (!TriggerScript.canMoveLeft && !TriggerScript.canMoveRight && TriggerScript.canMoveUp && TriggerScript.canMoveDown)
            {
                RandomNumber = Random.Range(0, 1);
                if (RandomNumber == 0)
                {
                    MoveUp();
                }
                else
                {
                    MoveDown();
                }
            }

            else if (TriggerScript.canMoveLeft && !TriggerScript.canMoveRight && !TriggerScript.canMoveUp && TriggerScript.canMoveDown)
            {
                RandomNumber = Random.Range(0, 1);
                if (RandomNumber == 0)
                {
                    MoveLeft();
                }
                else
                {
                    MoveDown();
                }
            }

            else if (TriggerScript.canMoveLeft && TriggerScript.canMoveRight && TriggerScript.canMoveUp && !TriggerScript.canMoveDown)
            {
                RandomNumber = Random.Range(0, 2);
                if (RandomNumber == 0)
                {
                    MoveLeft();
                }
                else if (RandomNumber == 1)
                {
                    MoveRight();
                }
                else
                {
                    MoveUp();
                }
            }

            else if (TriggerScript.canMoveLeft && !TriggerScript.canMoveRight && TriggerScript.canMoveUp && TriggerScript.canMoveDown)
            {
                RandomNumber = Random.Range(0, 2);
                if (RandomNumber == 0)
                {
                    MoveLeft();
                }
                else if (RandomNumber == 1)
                {
                    MoveDown();
                }
                else
                {
                    MoveUp();
                }
            }

            else if (TriggerScript.canMoveLeft && TriggerScript.canMoveRight && !TriggerScript.canMoveUp && TriggerScript.canMoveDown)
            {
                RandomNumber = Random.Range(0, 2);
                if (RandomNumber == 0)
                {
                    MoveLeft();
                }
                else if (RandomNumber == 1)
                {
                    MoveRight();
                }
                else
                {
                    MoveDown();
                }
            }

            else if (!TriggerScript.canMoveLeft && TriggerScript.canMoveRight && TriggerScript.canMoveUp && TriggerScript.canMoveDown)
            {
                RandomNumber = Random.Range(0, 2);
                if (RandomNumber == 0)
                {
                    MoveDown();
                }
                else if (RandomNumber == 1)
                {
                    MoveRight();
                }
                else
                {
                    MoveUp();
                }
            }

            else
            {
                RandomNumber = Random.Range(0, 3);
                if (RandomNumber == 0)
                {
                    MoveDown();
                }
                else if (RandomNumber == 1)
                {
                    MoveRight();
                }
                else if (RandomNumber == 2)
                {
                    MoveDown();
                }
                else
                {
                    MoveUp();
                }
            }

            TriggerScript.canMoveDown = false;
            TriggerScript.canMoveLeft = false;
            TriggerScript.canMoveRight = false;
            TriggerScript.canMoveUp = false;
            LoopCompleted = true;
        }
    }

    public void ReArrange()
    {
        GrabbedGold = false;
        //bring hero to initial position
        this.gameObject.transform.position = new Vector3(-4.43f, -3.85f, -3.294332f);
        //turnoff visible objects on board
        GO = GameObject.FindGameObjectsWithTag("Wumpus");
        foreach (GameObject i in GO)
        {
            Color tomp = i.GetComponent<SpriteRenderer>().color;
            tomp.a = 0f;
            i.GetComponent<SpriteRenderer>().color = tomp;
            i.GetComponent<BoxCollider>().enabled = true;
        }
        GO = GameObject.FindGameObjectsWithTag("Pit");
        foreach (GameObject i in GO)
        {
            Color tomp = i.GetComponent<SpriteRenderer>().color;
            tomp.a = 0f;
            i.GetComponent<SpriteRenderer>().color = tomp;
        }
        GO = GameObject.FindGameObjectsWithTag("Stench");
        foreach (GameObject i in GO)
        {
            Color tomp = i.GetComponent<SpriteRenderer>().color;
            tomp.a = 0f;
            i.GetComponent<SpriteRenderer>().color = tomp;
        }
        GO = GameObject.FindGameObjectsWithTag("Breeze");
        foreach (GameObject i in GO)
        {
            Color tomp = i.GetComponent<SpriteRenderer>().color;
            tomp.a = 0f;
            i.GetComponent<SpriteRenderer>().color = tomp;
        }
        GameObject.FindGameObjectWithTag("Gold").GetComponent<BoxCollider>().enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        Color tmp = other.GetComponent<SpriteRenderer>().color;
        tmp.a = 1f;
        other.GetComponent<SpriteRenderer>().color = tmp;

        if (other.gameObject.tag=="Wumpus"||other.gameObject.tag=="Pit")
        {
            //Application.LoadLevel(Application.loadedLevel);
            ReArrange();
            if(i==0)
            {
                StartCoroutine(RunAI());
                i++;
            }
        }

        if (other.gameObject.tag == "Gold")
        {
            GrabbedGold = true;
            Debug.Log("Yes it's Gold");
            GameObject.Find("Alerts").GetComponent<Text>().text = "Yes it's Gold";
            score += GoldScore;
            GameObject.Find("Score").GetComponent<Text>().text = score.ToString();
            Color temp = other.GetComponent<SpriteRenderer>().color;
            temp.a = 0f;
            other.GetComponent<SpriteRenderer>().color = temp;
            other.GetComponent<BoxCollider>().enabled = false;
        }

        if (other.gameObject.tag == "Breeze")
        {
            Debug.Log("Feel that Breeze?");
            GameObject.Find("Alerts").GetComponent<Text>().text = "Feel that Breeze?";
        }

        if (other.gameObject.tag == "Stench")
        {
            Debug.Log("Stinky,isn't it?");
            GameObject.Find("Alerts").GetComponent<Text>().text = "Stinky,isn't it?";
        }
    }
}
